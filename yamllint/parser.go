package yamllint

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

func ParseResult(fname string) ([]YamlLintReport, error) {
	var result []YamlLintReport

	reader, err := os.Open(fname)
	if err != nil {
		return nil, err
	}
	defer reader.Close()

	fscan := bufio.NewScanner(reader)
	fscan.Split(bufio.ScanLines)

	var lines []string
	for fscan.Scan() {
		lines = append(lines, fscan.Text())
	}

	for _, line := range lines {
		p, err := parse(line)
		if err != nil {
			return nil, err
		}
		result = append(result, p)
	}

	return result, nil
}

func parse(line string) (YamlLintReport, error) {
	var result YamlLintReport

	re := regexp.MustCompile(`^(.*):(\d+):(\d+):\s+\[(\w+)\]\s+(.+)\s+\((.+)\)$`)

	groups := re.FindStringSubmatch(line)
	if len(groups) == 7 {
		fname, lineno, charno, sev, desc, rname := groups[1], groups[2], groups[3], groups[4], groups[5], groups[6]
		line, err := strconv.Atoi(lineno)
		if err != nil {
			return YamlLintReport{}, err
		}
		pos, err := strconv.Atoi(charno)
		if err != nil {
			return YamlLintReport{}, err
		}
		result = YamlLintReport{
			Fname:  fname,
			Line:   line,
			Pos:    pos,
			Level:  sev,
			Detail: desc,
			Rule:   rname,
		}
	} else {
		fmt.Println("Invalid line: ignored")
	}

	return result, nil
}
