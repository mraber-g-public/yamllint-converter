package yamllint

// YamlLintReport describes yamllint output with 'parsable' format
type YamlLintReport struct {
	Fname  string
	Line   int
	Pos    int
	Level  string
	Detail string
	Rule   string
}
