SHELL=/bin/bash

MAIN_PACKAGE = ./cmd/glab-cc-converter
MAIN_BIN = bin/$(notdir $(basename ${MAIN_PACKAGE}))
MAIN_VERSION ?= $(shell git describe --tags 2>/dev/null || git rev-parse --short HEAD)
BUILD_DATE ?= $(shell date "+%Y-%m-%d")


PACKAGES ?= $(shell go list ./...)
GO_LDFLAGS := $(GO_LDFLAGS) -X "main.version=$(MAIN_VERSION) ($(BUILD_DATE))"

.DEFAULT_GOAL:=help

HASGOTESTSUM := $(shell which gotestsum 2> /dev/null)
HASGOCILINT := $(shell which golangci-lint 2> /dev/null)

# short-verbose
TEST_FORMAT = short
ifdef HASGOTESTSUM
    GOTEST=gotestsum
else
    GOTEST=bin/gotestsum
endif

ifdef HASGOCILINT
    GOLINT=golangci-lint
else
    GOLINT=bin/golangci-lint
endif

# Dependency versions
GOTESTSUM_VERSION = 1.12.0
GOLANGCI_VERSION = 1.61.0

OS = $(shell uname | tr A-Z a-z)

##@ General:
# ------------------------------------------------------------------------------
.PHONY: help
help:                                               ## Display this help message
	$(info Helper to build and test the project.)
	@awk \
		'BEGIN { \
			FS = ":.*##"; \
			printf "\nUsage:\n  make \033[36m<target>\033[0m\n" \
		} \
		/^[a-zA-Z0-9_-]+:.*?##/ { \
			printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 \
		} \
		/^##@/ { \
			printf "\n\033[1m%s\033[0m\n", substr($$0, 5) \
		} \
		' $(MAKEFILE_LIST)

##@ Building:
# ------------------------------------------------------------------------------
${MAIN_BIN}: build

.PHONY: build
build:                                                 ## Produce the executable
	@go build -ldflags '$(GO_LDFLAGS)' -trimpath -o ${MAIN_BIN} ${MAIN_PACKAGE}

.PHONY: run
run:                                      ## Compile and execute the application
	go run -ldflags '$(GO_LDFLAGS)' -trimpath ${MAIN_PACKAGE}

##@ Tests:
# ------------------------------------------------------------------------------
ifdef HASGOTESTSUM
bin/gotestsum:
	@echo "Skip this"
else
bin/gotestsum: bin/gotestsum-${GOTESTSUM_VERSION}
	@ln -sf gotestsum-${GOTESTSUM_VERSION} bin/gotestsum
endif

bin/gotestsum-${GOTESTSUM_VERSION}:
	@mkdir -p bin
	curl -sfL \
		https://github.com/gotestyourself/gotestsum/releases/download/v${GOTESTSUM_VERSION}/gotestsum_${GOTESTSUM_VERSION}_${OS}_amd64.tar.gz \
		| tar -zOxf - gotestsum > ./bin/gotestsum-${GOTESTSUM_VERSION} \
		&& chmod +x ./bin/gotestsum-${GOTESTSUM_VERSION}

.PHONY: test
test: bin/gotestsum                                            ## Run Unit Tests
	# $(GOTEST) -v -race ./...
	$(GOTEST) --format ${TEST_FORMAT} -- -v ./...

.PHONY: test-ci
test-ci: TEST_FORMAT ?= short        ## Create tests and coverage reports for CI
test-ci: bin/gotestsum
		$(GOTEST) \
			--jsonfile test-output.log \
			--no-summary=skipped \
			--junitfile ./coverage.xml --format ${TEST_FORMAT} --\
			-coverprofile=./coverage.txt -covermode=atomic \
			./...

.PHONY: cover-summary
cover-summary:                         ## Display coverage report summary for CI
	@go tool cover -func coverage.txt

.PHONY: cover-html
cover-html:                               ## Display interactive coverage report
	@go test -v -race -coverprofile=/tmp/coverage.txt ./...
	@go tool cover -html=/tmp/coverage.txt

##@ Release:
# ------------------------------------------------------------------------------
.PHONY: check
check: test lint                                        ## Run tests and linters

.PHONY: dist
dist: check build                                         ## Create distribution
	$(info Creating release...)
	$(info Done.)

.PHONY: fmt
fmt:                                                       ## Format source code
	@go fmt ./...

ifdef HASGOCILINT
bin/golangci-lint:
	@echo "Skip this"
else
bin/golangci-lint: bin/golangci-lint-${GOLANGCI_VERSION}
	@ln -sf golangci-lint-${GOLANGCI_VERSION} bin/golangci-lint
endif

bin/golangci-lint-${GOLANGCI_VERSION}:
	@mkdir -p bin
	curl -sfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh \
		| bash -s -- -b ./bin v${GOLANGCI_VERSION}
	@mv bin/golangci-lint $@

.PHONY: lint
lint: bin/golangci-lint                                           ## Run linters
	$(GOLINT) run

.PHONY: tidy
tidy:                                                ## Format and clean modfile
	@go mod tidy -v
	@go fmt ./...

##@ Tools:
# ------------------------------------------------------------------------------
.PHONY: clean
clean:                                                   ## Remove working files
	$(info Cleaning project...)
	@go clean
	@rm -f ${MAIN_BIN}

.PHONY: install
install:                                              ## Install the application
	@go install -ldflags '$(GO_LDFLAGS)' -trimpath ${MAIN_PACKAGE}

.PHONY: list-todo
list-todo: bin/golangci-lint    ## Detect FIXME, TODO and other comment keywords
	$(GOLINT) run --enable=godox --disable-all --issues-exit-code 0
