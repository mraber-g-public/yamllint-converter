# Notes

Convert `yamllint` output to a format accepted by GitLab "code quality".

Should also convert `markdownlint` report.

See:

* [Gilab](https://docs.gitlab.com/ee/ci/testing/code_quality.html#implement-a-custom-tool)
* [CodeClimate](https://github.com/codeclimate/platform/blob/master/spec/analyzers/SPEC.md#data-types)

TODO

* [x] Add Makefile
* [ ] Add GitLab CI
* [ ] Add parameters
* [ ] Add Unit Tests
* [ ] Manage errors
* [ ] Publish to Docker Hub
* [ ] Write documation

RÉCUPERER LE 1ER COMMIT FAIT PAR GITLAB À L'INIT DU PROJET
=> CRÉER UN AUTRE PROJET BIDON !!
