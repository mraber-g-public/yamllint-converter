package glabcc

// Lines refers to the block of code containing the issue
type Lines struct {
	// begin is the index of the first line (1-based, inclusive)
	Begin int `json:"begin"`
	// end is the index of the last line (1-based, inclusive)
	End int `json:"end,omitempty"`
}

// Coord refers to a specific character in a file
type Coord struct {
	Line   int `json:"line"`
	Column int `json:"column,omitempty"`
}

// Positions represents the source code range containing the issue
type Positions struct {
	// Begin refers to the 1st character
	Begin Coord `json:"begin,omitempty"`
	// End refers to the last character
	End Coord `json:"end,omitempty"`
}

// Location represents the place in the source code tree where the issue appears
type Location struct {
	// path is the relative path to the file containing the issue
	Path string `json:"path"`
	// lines describes the block of code containing the issue
	Lines     *Lines     `json:"lines,omitempty"`
	Positions *Positions `json:"positions,omitempty"`
}

// Issue describes a violation
type Issue struct {
	Description string   `json:"description"`
	CheckName   string   `json:"check_name"`
	FingerPrint string   `json:"fingerprint"`
	Severity    string   `json:"severity"`
	Location    Location `json:"location"`
}

// Report describes all violations
type Report []Issue
