package main

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/mraber-g-public/glab-cc-convert/glabcc"
	"gitlab.com/mraber-g-public/glab-cc-convert/yamllint"
)

// Set at build time
var version string = "local"

func main() {
	fmt.Printf("Running version: %v\n", version)
	err := run()
	if err != nil {
		os.Exit(1)
	}
}

func run() error {
	// Plan:
	// - Read input file ("parsable" format)
	// - Create JSON structure
	// - For each input_line:
	//   - Split line
	//	 - Append JSON object
	// - Serialize JSON struct as output_file

	// TEST (json)
	r := glabcc.Report{
		{
			Description: "foo",
			CheckName:   "C001",
			FingerPrint: "abcdd30330303",
			Severity:    "error",
			Location: glabcc.Location{
				Path: "tmp/foo.go",
				Lines: &glabcc.Lines{
					Begin: 19,
					End:   19,
				},
			},
		},
		{
			Description: "bar",
			CheckName:   "C002",
			FingerPrint: "zzresdfsfrovcmmdu4831",
			Severity:    "warning",
			Location: glabcc.Location{
				Path: "tmp/bar.go",
				Lines: &glabcc.Lines{
					Begin: 1,
					End:   0,
				},
			},
		},
	}
	data, _ := json.MarshalIndent(r, "", "  ")
	fmt.Println(string(data))

	// READ file
	ylReport, _ := yamllint.ParseResult("./data/out.parsable")
	fmt.Println(ylReport)

	// CONVERT
	report := make(glabcc.Report, 0)
	for _, p := range ylReport {

		report = append(report, glabcc.Issue{
			Description: p.Detail,
			CheckName:   p.Rule,
			FingerPrint: "nnnnnn",
			Severity:    p.Level,
			Location: glabcc.Location{
				Path: p.Fname,
				Lines: &glabcc.Lines{
					Begin: p.Line,
				},
			},
		})
	}

	// JSON
	fmt.Println("========================================================")
	data2, _ := json.MarshalIndent(report, "", "  ")
	fmt.Println(string(data2))

	return nil
}
